(function($){

	var asketic = asketic || {};
	
	asketic.HandsOnTable = function()
	{ 
		var self = {};
		
		self.$tables = $();
		
		self.save = function() {		
			self.$tables.each(function(){
				var $eachTable = $(this);
				var $hiddenField = $eachTable.parents(".handsontable-form").find('input[type="hidden"]');
				var data = $eachTable.data('handsontable').getData();
				var strData = $.toJSON( data );
				$hiddenField.val( strData );							
			});
		}
		
		self.refresh = function() {
			var $table = $(".handsontable:not(.asketic-table-processed)").addClass('asketic-table-processed');		
			if($table.length)		
			{
				$table.each(function(){		
					var $eachTable = $(this);	
										
					//For CCK multiple fields
					$eachTable.parents('td:first').css('width', '99%');
					
					self.$tables = self.$tables.add($eachTable);
					
					$eachTable.handsontable({
						    rows: 2,
						    cols: 2,
						    minSpareCols: 1, //always keep at least 1 spare row at the right
						    minSpareRows: 1, //always keep at least 1 spare row at the bottom
						    contextMenu: true,
						    legend: [
						      {
						        match: function (row, col, data) {
						          return (row === 0); //if it is first row
						        },
						        style: {	          
						          fontWeight: 'bold'
						        },
						        title: 'Heading' //make some tooltip
						      }
						     ]
					  });
					
					 var $hiddenField = $eachTable.parents(".handsontable-form").find('input[type="hidden"]');
					 
					 var data = [];
					 var strData = $hiddenField.val();
					 if(strData.length)
					 {
					 	data = $.evalJSON(strData);
					 }									
					 
			  		 $eachTable.handsontable("loadData", data);
			  		 
			  		 //Handle multiple CCK fields
			  		 var $cckMultipleTable = $eachTable.parents("table.content-multiple-table");
			  		 if($cckMultipleTable.length)
			  		 {
			  		 	var $submitButton = $cckMultipleTable.parent().find('.form-submit:not(.asketic-table-processed)');
			  		 	$submitButton.addClass('asketic-table-processed').bind('mousedown keypress').mousedown(function(){									
							self.save();
						});	
			  		 }
			  });
		  	}
	 	}
		self.refresh();
		
	   	/* Destruct instance */
	    self.release = function()
	    {    				
			
	    }
	
	    return self;
	};
	
	$(document).ready(function(){
		$('form#node-form').submit(function(event){		
			window.handsOnTable.save();
		});
		
		window.handsOnTable = new asketic.HandsOnTable();
		
		Drupal.behaviors.HandsOnTable = function() {
			window.handsOnTable.refresh();
		}
	});
	
})(jQuery_1_7_2);
